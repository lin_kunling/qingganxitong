# coding=utf-8
import numpy as np

class dara_pre(object):
    '''
    该函数用于对数据做预处理。
    input：H（心情向量）。输入向量格式。自行输入心情向量，每个数字代表一个特征对应的分数。最大为10，最小为-10
                       按照【语音、表情、行为、机器人本身环境】4个领域格式。
           Hd（心情向量每个数字对应的领域）。写出H是属于哪个领域，1代表语音，2代表表情，3代表行为,4代表机器人本身环境
           C1（可分类特征矩阵），输入矩阵格式，每一行代表一个特征不同类别的等级.最大为4，最小为-4
           C2（不可分类特征矩阵），输入格式为向量，每个数字代表每个特征的等级.最大为4，最小为-4
           P（亲密度特征比例），输入格式为向量，输入格式为比率。
           Cd（亲密特征每个特征对应的领域）。写出C1和C2是属于哪个领域，1代表语音，2代表表情，3代表行为,4代表机器人本身环境
    :return:
           h（心情特征向量），c（亲密特征矩阵），p（亲密度特征比例）
    '''
    def __init__(self, H, Hd, C1, C2, Cd, P):
        self.H = H
        self.Hd = Hd
        self.Hd_score = []
        self.C1 = C1
        self.C2 = C2
        self.P = P
        self.Cd = Cd
        self.Cd_score = []
        self.mix_vec=()

        self.de_h()
        self.de_c()
        self.mix_h_c_fra()
        self.mix_h_c_vec()


    def de_h(self):
        '''
        这个函数主要用于对心情向量H做心情预处理
        input：H
        :return: h
        '''
        #计算心情值
        self.h_sum = sum(self.H)
        #print(self.h_sum)

        #计算心情领域矩阵
        l1, l2, l3, l4 = 0, 0, 0, 0
        for i in range(len(self.H)):
            if self.Hd[i] == 1: l1 = l1 + self.H[i]
            if self.Hd[i] == 2: l2 = l2 + self.H[i]
            if self.Hd[i] == 3: l3 = l3 + self.H[i]
            if self.Hd[i] == 4: l4 = l4 + self.H[i]
        self.Hd_score = [l1, l2, l3, l4]
        #print(self.Hd_score)
        return self.Hd_score

    def de_c(self):
        '''
        这个函数主要用于对亲密矩阵做预处理
        input：C1，C2
        :return:c
        '''
        #求出可分类特征中最多类别的维度
        #print(C1)
        max_le = 0
        for li in self.C1:
            a = len(li)
            if a > max_le:
                max_le = a
        #print(a,len(self.C1))

        #创建n*c的零矩阵，n为特征叔，c为最多类别的维度
        self.c = np.zeros((len(self.C1) + len(self.C2), a))

        #把特征填入到零矩阵中
        for i in range(len(self.C1)):#填入可分类特征
            a1 = self.C1[i]
            for j in range(len(a1)):
                self.c[i, j] = a1[j]
        for k in range(len(self.C2)):#填入不可分类特征
            self.c[k + len(self.C1), 0] = self.C2[k]
        #print(self.c)

        #计算亲密领域矩阵
        l1, l2, l3, l4 = 0, 0, 0, 0
        for l in range(len(self.c)):
            if self.Cd[l] == 1: l1 = l1 + sum(self.c[l])
            if self.Cd[l] == 2: l2 = l2 + sum(self.c[l])
            if self.Cd[l] == 3: l3 = l3 + sum(self.c[l])
            if self.Cd[l] == 4: l4 = l4 + sum(self.c[l])
        self.Cd_score = [l1, l2, l3, l4]
        #print(self.Cd_score)
        return self.Cd_score


    def maxminnor(self, x, min, max):
        '''
        该函数用于对向量或者矩阵做【0，1】标准化
        :param x:需要标准化的list或矩阵
        :param min:输入的向量或矩阵中的最小值
        :param max:输入的向量或矩阵中的最大值
        :return:标准化后的向量或矩阵
        '''
        nor_li = np.zeros(self.c.shape)
        for i in range(self.c.shape[0]):
            for j in range(self.c.shape[1]):
                num = self.c[i,j]
                if num != 0:
                    nor_li[i,j] = (num - min)/(max-min)
        #print(nor_li)


    def mix_h_c_fra(self):
        '''
        该函数用于合并心情向量与亲密度矩阵，用于监督学习。
        合成一个矩阵
        input：H，c
        :return:hc
        '''
        #对心情向量做数据预处理，让心情向量和亲密度矩阵最大最小值达成一致
        self.h = np.array(self.H) /2.5

        #融合心情向量和亲密度矩阵
        max_c = max(self.c.shape[1], self.h.shape[0])
        self.hc = np.zeros((self.c.shape[0]+ 1, max_c))

        for i in range(self.c.shape[0]):
            for j in range(self.c.shape[1]):
                self.hc[i, j] = self.c[i,j]

        for k in range(self.hc.shape[1]):
            self.hc[-1, k] = self.h[k]

        #print(self.hc)
        return self.hc
        #print(self.Hd_score,self.Cd_score)

    def mix_h_c_vec(self):
        '''
        该函数用于合并心情向量与亲密度矩阵，用于监督学习。
        合成一个向量
        :return:
        '''
        #print(self.C1, self.C2, self.h)
        a =[]
        for i in range(len(self.C1)):
            da = self.C1[i]
            for j in range(len(da)):
                a.append(da[j])

        for k in range(len(self.C2)):
            a.append(self.C2[k])

        h = list(self.h)
        for p in range(len(self.h)):
            a.append(h[p])
        #print(a)
        self.mix_vec = a
        #print(self.mix_vec)
        return self.mix_vec

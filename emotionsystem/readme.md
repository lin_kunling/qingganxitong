# 情感系统 2.0版本
## 监督算法

监督算法要求有训练集。
目前是多种算法结合，分别是C4.5、SVM、randomtree、Xgboost模型


### train
#### input
监督算法要求有训练集，格式如data/test_data.xls。即训练模型要输入特征数据与反馈标签数据。

#### output
训练输出为训练好的模型。
#### code
训练代码格式如下：
'''
from model import module
model = module()
model.run(features, labels, method, model_name)
'''
features——输入的特征矩阵
labels——输入的反馈矩阵
method——训练方法，目前有'c45','svm','xgb','rf','all',前四种分别代表C4.5、SVM、randomtree、Xgboost模型，'all'则代表4种结合，取准确率最高的模型。
model_name——保存模型的名字

demo如下：
'''
# 导入模块
import pandas as pd
from model import module
from unsu_model import unsu_module

#数据预处理
raw_data = pd.read_csv('data/test_data.csv', header=0)
data = raw_data.values
labels = data[::, 0]
features = data[::, 1::]

#训练模型
model = module()
model.run(features, labels, 'all', 'model/7-6model')
'''

### test
测试时要输入特征数据，将返回反馈标签数据。

#### input:
要输入特征数据，可以向量或者是list的形式输入
分数向量（包括心情与亲密度分数）,每个数据代表一个特征
如：[0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0]

分数list，多个人的分数向量
如：[[0, 0, -1, 1, 1, 1, -2, 0, 0, 2, -1, 0 ] ,
        [0, 0, 1, 2, 2, 2, 0, -2, -2, 0 ,1, 0] ,
        [0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0 ]]
        

#### output
输出是一个反馈的等级，目前共有三个等级，分别是1 2 3 

#### code
测试代码格式如下：
'''
from model import module
model = module()
y_predict = model.test_tree(test_features,model_name)
'''
test_features——测试集特征
model_name——测试模型名字

demo如下：
'''
from model import module
model = module()
test3 = [0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0]
y_predict3 = model.test_tree([test3], 'model/7-6model')
print( y_predict3)
'''

## 非监督算法
非监督算法是基于一条已经定义好的公式进行计算反馈的，这个算法不需要训练，没有训练数据，可用于无训练数据的情况。

#### input
分数向量（包括心情与亲密度分数）,每个数据代表一个特征
如：[0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0]

分数list，多个人的分数向量
如：[[0, 0, -1, 1, 1, 1, -2, 0, 0, 2, -1, 0 ] ,
        [0, 0, 1, 2, 2, 2, 0, -2, -2, 0 ,1, 0] ,
        [0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0 ]]

#### output
输出是一个反馈的等级，目前共有三个等级，分别是1 2 3 

#### code 
计算反馈代码如下：
'''
from unsu_model import unsu_module
un_model = unsu_module()
un_model.heart_qinmi(test, c, cd, p)
'''
test——为要测试的用户特征向量或者list
c——test中对应位置的特征类别。0为亲密度分数，1为心情度分数
cd——test中对应位置的特征领域。1代表语音，2代表表情，3代表行为,4代表机器人本身环境
p——亲密度特征比例

demo如下：
'''
from unsu_model import unsu_module
test3 = [0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0]
c = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1]#0为亲密度分数，1为心情度分数
cd = [1, 1, 2, 3, 1, 2, 1, 1, 2, 4, 3, 4]#写出c中每个向量是属于哪个领域，1代表语音，2代表表情，3代表行为,4代表机器人本身环境
p = [1, 1, 2, 1, 1, 1]
un_model = unsu_module()
un_model.heart_qinmi(test3, c, cd, p)

'''
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 16:23:48 2020

@author: Colleen
"""

import pandas as pd
from model import module
from unsu_model import unsu_module

#数据预处理
raw_data = pd.read_csv('data/test_data.csv', header=0)
data = raw_data.values
labels = data[::, 0]
features = data[::, 1::]

######################监督模型
#训练模型
model = module()
model.run(features, labels, 'all', 'model/7-6model')

#测试模型
model = module()
test3 = [0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0]
y_predict3 = model.test_tree([test3], 'model/7-6model')
print( y_predict3)

test = [[0, 0, -1, 1, 1, 1, -2, 0, 0, 2, -1, 0 ] ,
        [0, 0, 1, 2, 2, 2, 0, -2, -2, 0 ,1, 0] ,
        [0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0 ]]
#print(type(test[0][0]))
y_predict = model.test_tree(test, '6-23model' )

#########################非监督模型
from unsu_model import unsu_module
test3 = [0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0]
c = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1]#0为亲密度分数，1为心情度分数
cd = [1, 1, 2, 3, 1, 2, 1, 1, 2, 4, 3, 4]#写出c中每个向量是属于哪个领域，1代表语音，2代表表情，3代表行为,4代表机器人本身环境
p = [1, 1, 2, 1, 1, 1]
un_model = unsu_module()
un_model.heart_qinmi(test3, c, cd, p)


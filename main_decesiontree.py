# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 16:23:48 2020

@author: Colleen
"""

import pandas as pd
from model import module
from unsu_model import unsu_module

#数据预处理
raw_data = pd.read_csv('test_data.csv', header=0)
data = raw_data.values
labels = data[::, 0]
features = data[::, 1::]

#训练模型
model = module()
model.run(features, labels, 'all', '6-23model')

#非监督模型
from unsu_model import unsu_module
un_model = unsu_module()
#un_model.heart(test3, c, cd)
#un_model.heart(test, c, cd)

#un_model.qinmi(test3, c, cd, p)
#un_model.qinmi(test, c, cd, p)
un_model.heart_qinmi(test, c, cd, p)
#测试模型
test1 = [0, 0, -1, 1, 1, 1, -2, 0, 0, 2, -1, 0]
test2 = [0, 0, 1, 2, 2, 2, 0, -2, -2, 0, 1, 0]

test3 = [0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0]

c = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1]#0为亲密度分数，1为心情度分数
cd = [1, 1, 2, 3, 1, 2, 1, 1, 2, 4, 3, 4]#写出c中每个向量是属于哪个领域，1代表语音，2代表表情，3代表行为,4代表机器人本身环境
p = [1, 1, 2, 1, 1, 1]
print(type(c[0][0]))

model = module()
y_predict1 = model.test_tree([test1], '6-23model')
y_predict2 = model.test_tree([test2], '6-23model')
y_predict3 = model.test_tree([test3], '6-23model')
print(y_predict1, y_predict2, y_predict3)

test = [[0, 0, -1, 1, 1, 1, -2, 0, 0, 2, -1, 0 ] ,
        [0, 0, 1, 2, 2, 2, 0, -2, -2, 0 ,1, 0] ,
        [0, 0, 1, 1, 1, 2, 0, 0, 0, 2, -1, 0 ]]
print(type(test[0][0]))
y_predict = model.test_tree(test, '6-23model' )
print(y_predict)

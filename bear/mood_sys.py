#!/usr/bin/env python
# coding: utf-8

# # 机器人的心情系统
# #### 心情系统由以下因素组成（开机基础分数为60）：
# 1、环境因素c_weather：天气状态（多云、下雨、刮风心情-5，晴天心情+5）
# 
# 2、电量c_electricity：电量直接影响心情（>50%:+5，20%-50%：0，<20%:-5）
# 
# 3、当前动作状态c_action：倒下、躺下没人理

# In[20]:


# -*- coding:utf-8 -*
import requests

class mood_sys():
    '''
    此代码用于模拟机器人的心情系统，主要由以下五点组成：
    1、环境因素c_weather：天气状态（多云、下雨、刮风心情-5，晴天心情+5）
    2、电量c_electricity：电量直接影响心情（>50%:+10，20%-50%：0，<20%:-10）
    3、当前动作状态c_action：只有两个分类，正与不正，正+10，不正-5。
    4、获取人脸识别中的表情，实时获取。
    5、获取主人抚摸的位置，若抚摸头部或者手部，或者抱起，就会加分。
    6、特殊加分项：
    '''
    def __init__(self):
        self.base_socre = 60
        self.c_electricity = 69  #电量要自己获取
        self.weather_score = 0
        self.ele_score = 0
        self.action_score = 0
        self.exp_score = 0
        self.torch_score = 0

    def get_weather_score(self):
        rep = requests.get('http://www.tianqiapi.com/api?version=v6&appid=23035354&appsecret=8YvlPNrz&city=深圳')
        rep.encoding = 'utf-8'
        wea = rep.json()['wea']
        air = rep.json()['air_level']
        if wea == '晴' : self.weather_score = self.weather_score + 5# 喜欢晴天
        if '雨' in wea : self.weather_score = self.weather_score - 10# 不喜欢雨天
            
        if air == '优': self.weather_score = self.weather_score + 5 
        elif '污染' in air : self.weather_score = self.weather_score - 5
        #print(wea, air)
    
    def get_ele_score(self):
        if self.c_electricity > 50 : self.ele_score = self.ele_score + 10
        elif  self.c_electricity > 20 : self.ele_score = self.ele_score + 0
        else : self.ele_score = self.ele_score - 10

    def get_action_score(self):
        c_action = 1 #这个需要获取，0代表不正，1代表正
        if c_action == 1: self.action_score = self.action_score + 10
        else : self.action_score = self.action_score - 5
            
    def get_expresstion(self):
        '''
        获取人脸识别中的表情，实时获取。
        共有十种表情："angry":"愤怒","disgust":"厌恶","fear":"恐惧", "happy":"高兴", "sad":"伤心",
                      "surprise":"惊讶", "neutral":"淡定","pouty":"撅嘴","grimace":"鬼脸"
        若表情是happy高兴、surprise惊讶、grimace鬼脸则加分，若是angry愤怒、disgust厌恶则扣分。
        '''
        expression = 'happy'
        add_exp = ['happy', 'surprise', 'grimace']
        cut_exp = ['angry', 'disgust']
        if expression in add_exp: self.exp_score = self.exp_score + 5
        if expression in cut_exp: self.exp_score = self.exp_score +-5
            
        
    def get_torch_section(self):
        '''
        获取主人抚摸的位置，若抚摸头部或者手部，或者抱起，就会加分。
        '''
        section = 'head' #要获取
        like = ['head', 'hand', 'hug']
        unlike = ['hip']
        if section in  like : self.torch_score = self.torch_score + 5
        if section in unlike : self.torch_score = self.torch_score - 5
    
    def get_mood_count(self):
        self.get_weather_score()
        self.get_ele_score()
        self.get_action_score()
        self.get_expresstion()
        self.get_torch_section()
        self.mood_count = self.base_socre + self.weather_score + self.ele_score + self.action_score + self.exp_score  + self.torch_score#线性计算
        #print(self.base_socre,  self.weather_score, self.ele_score, self.action_score, self.exp_score, self.torch_score)
        print('心情系数：',self.mood_count)
        return self.mood_count







